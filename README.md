# GitLab

My DotFiles

## Requirements

- xmonad
- xmonad-contrib
- xmobar
- kitty
- dbus-x11
- dbus
- SauceCode Pro Nerd Font Mono
- mononoki Nerd Font
- Ubuntu Font
- [Picom by Ibhagwan](https://github.com/ibhagwan/picom)
- Exa Shell
- Bat
- Fish
- [shell-color-script](https://gitlab.com/dwt1/shell-color-scripts)
- StarShip Prompt
- lolcat

## Installation

**Debian**

`sudo apt install xmonad libghc-xmonad-contrib-dev xmobar kitty dbus-x11 dbus fonts-ubuntu`

Follow Nerd Font Installation Guild To Install `Mononoki` And `SauceCode Pro`
